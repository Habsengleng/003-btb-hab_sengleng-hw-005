class OutputString extends Thread{
    private String st;
    private int time;
    public OutputString(String st,int time){
        this.st=st;
        this.time=time;
    }

    @Override
    public void run() {
            printString(st, time);
    }

    void  printString(String st, int time) {
        for (int i = 0; i < st.length(); i++) {
            System.out.print(st.charAt(i));
            try {
                Thread.sleep(time);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println();
    }
}
class OutputDifferentSpeed extends Thread {
    String str;
    int speed;
    public OutputDifferentSpeed(String str,int speed){
        this.str=str;
        this.speed=speed;
    }

    @Override
    public void run() {
       newSpeed(str,speed);
    }
   public static void newSpeed(String st,int speed) {
       String[] str = st.split("\\s");
       System.out.print(str[0]);
       for(int i=0;i<str[1].length();i++){
           System.out.print(str[1].charAt(i));
           try{
               Thread.sleep(speed);
           }catch (Exception e){
               System.out.println(e);
           }
       }
       System.out.print(str[2]);
       System.out.print(" "+str[3]);
   }
}
public class Greeting {
    public static String printSymbol(String ch,int lenght){
        for(int i=0;i<lenght;i++){
            ch+=ch.charAt(i);
        }
        return ch;
    }
    public static void main(String[] args) throws InterruptedException{
        String st1 = "Hello KSHRD!";
        String st2 = "I will try my best to be here at HRD.";
        String st3 = "Downloading .......... Complete 100%";
        OutputString os = new OutputString(st1,250);
        OutputString os1 = new OutputString(printSymbol("*",40),200);
        OutputString os2 = new OutputString(st2,200);
        OutputString os3 = new OutputString(printSymbol("-",40),200);
        OutputDifferentSpeed od = new OutputDifferentSpeed(st3,250);
        os.start();
        os.join();
        os1.start();
        os1.join();
        os2.start();
        os2.join();
        os3.start();
        os3.join();
        od.start();
    }
}
